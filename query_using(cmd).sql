
/* Table and database are created  */
/*3.Create different-different Select query with DISTINCT,*,FROM, WHERE,GROUP BY,HAVING and HAVING*/

MariaDB [demo]> SELECT DISTINCT gender FROM members;
+--------+
| gender |
+--------+
| female |
| Male   |
+--------+
2 rows in set (0.000 sec)

MariaDB [demo]> SELECT  `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM members;
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
| membership_number | full_names        | gender | date_of_birth | physical_address       | postal_address | contact_number | email                |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
|                 1 | janes_notes       | female | 1980-07-21    | First Street Plot No 4 | Private Bag    |      759253542 | janetjones@yahoo.com |
|                 2 | janet smith jones | female | 1980-06-23    | Melrose 123            | NULL           |              1 | jj@fstreet.com       |
|                 3 | Robert Fill       | Male   | 1980-07-12    | 3rd street 34          | NULL           |          12345 | rm@street.com        |
|                 4 | Gloaria Williams  | female | 1984-02-14    | 2nd street 23          | NULL           |              1 | NULL                 |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
4 rows in set (0.001 sec)

MariaDB [demo]> SELECT `gender`,`membership_number` FROM members;
+--------+-------------------+
| gender | membership_number |
+--------+-------------------+
| female |                 1 |
| female |                 2 |
| Male   |                 3 |
| female |                 4 |
+--------+-------------------+
4 rows in set (0.000 sec)

MariaDB [demo]> SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM members  WHERE gender='female';
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
| membership_number | full_names        | gender | date_of_birth | physical_address       | postal_address | contact_number | email                |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
|                 1 | janes_notes       | female | 1980-07-21    | First Street Plot No 4 | Private Bag    |      759253542 | janetjones@yahoo.com |
|                 2 | janet smith jones | female | 1980-06-23    | Melrose 123            | NULL           |              1 | jj@fstreet.com       |
|                 4 | Gloaria Williams  | female | 1984-02-14    | 2nd street 23          | NULL           |              1 | NULL                 |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
3 rows in set (0.001 sec)

MariaDB [demo]> SELECT COUNT(membership_number), full_names FROM members GROUP BY gender;
+--------------------------+-------------+
| COUNT(membership_number) | full_names  |
+--------------------------+-------------+
|                        3 | janes_notes |
|                        1 | Robert Fill |
+--------------------------+-------------+
2 rows in set (0.001 sec)

MariaDB [demo]> SELECT COUNT(membership_number), gender FROM members GROUP BY gender HAVING COUNT(membership_number) > 2;
+--------------------------+--------+
| COUNT(membership_number) | gender |
+--------------------------+--------+
|                        3 | female |
+--------------------------+--------+
1 row in set (0.000 sec)

/*4.Create query using WHERE Clause(IN, OR, AND, Not IN,Equal To,Not Equal To, Greater than, less than )*/
MariaDB [demo]>


MariaDB [demo]> SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM `members` WHERE `membership_number` NOT IN (1,2,3);
+-------------------+------------------+--------+---------------+------------------+----------------+----------------+-------+
| membership_number | full_names       | gender | date_of_birth | physical_address | postal_address | contact_number | email |
+-------------------+------------------+--------+---------------+------------------+----------------+----------------+-------+
|                 4 | Gloaria Williams | female | 1984-02-14    | 2nd street 23    | NULL           |              1 | NULL  |
+-------------------+------------------+--------+---------------+------------------+----------------+----------------+-------+
1 row in set (0.001 sec)

MariaDB [demo]> SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM `members` WHERE `membership_number` IN (1,2,3);
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
| membership_number | full_names        | gender | date_of_birth | physical_address       | postal_address | contact_number | email                |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
|                 1 | janes_notes       | female | 1980-07-21    | First Street Plot No 4 | Private Bag    |      759253542 | janetjones@yahoo.com |
|                 2 | janet smith jones | female | 1980-06-23    | Melrose 123            | NULL           |              1 | jj@fstreet.com       |
|                 3 | Robert Fill       | Male   | 1980-07-12    | 3rd street 34          | NULL           |          12345 | rm@street.com        |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
3 rows in set (0.001 sec)

MariaDB [demo]> SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM `members` WHERE `membership_number` = 1 OR `full_names` = 'janes_notes';
+-------------------+-------------+--------+---------------+------------------------+----------------+----------------+----------------------+
| membership_number | full_names  | gender | date_of_birth | physical_address       | postal_address | contact_number | email                |
+-------------------+-------------+--------+---------------+------------------------+----------------+----------------+----------------------+
|                 1 | janes_notes | female | 1980-07-21    | First Street Plot No 4 | Private Bag    |      759253542 | janetjones@yahoo.com |
+-------------------+-------------+--------+---------------+------------------------+----------------+----------------+----------------------+
1 row in set (0.001 sec)

MariaDB [demo]> SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM `members` WHERE `membership_number` ='1' AND `gender` = 'female';
+-------------------+-------------+--------+---------------+------------------------+----------------+----------------+----------------------+
| membership_number | full_names  | gender | date_of_birth | physical_address       | postal_address | contact_number | email                |
+-------------------+-------------+--------+---------------+------------------------+----------------+----------------+----------------------+
|                 1 | janes_notes | female | 1980-07-21    | First Street Plot No 4 | Private Bag    |      759253542 | janetjones@yahoo.com |
+-------------------+-------------+--------+---------------+------------------------+----------------+----------------+----------------------+
1 row in set (0.002 sec)

MariaDB [demo]> SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM members WHERE  membership_number > 3 ;
+-------------------+------------------+--------+---------------+------------------+----------------+----------------+-------+
| membership_number | full_names       | gender | date_of_birth | physical_address | postal_address | contact_number | email |
+-------------------+------------------+--------+---------------+------------------+----------------+----------------+-------+
|                 4 | Gloaria Williams | female | 1984-02-14    | 2nd street 23    | NULL           |              1 | NULL  |
+-------------------+------------------+--------+---------------+------------------+----------------+----------------+-------+
1 row in set (0.000 sec)

MariaDB [demo]> SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM members WHERE  membership_number < 4 ;
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
| membership_number | full_names        | gender | date_of_birth | physical_address       | postal_address | contact_number | email                |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
|                 1 | janes_notes       | female | 1980-07-21    | First Street Plot No 4 | Private Bag    |      759253542 | janetjones@yahoo.com |
|                 2 | janet smith jones | female | 1980-06-23    | Melrose 123            | NULL           |              1 | jj@fstreet.com       |
|                 3 | Robert Fill       | Male   | 1980-07-12    | 3rd street 34          | NULL           |          12345 | rm@street.com        |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
3 rows in set (0.001 sec)

MariaDB [demo]> SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email`  FROM members WHERE membership_number <> 3;
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
| membership_number | full_names        | gender | date_of_birth | physical_address       | postal_address | contact_number | email                |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
|                 1 | janes_notes       | female | 1980-07-21    | First Street Plot No 4 | Private Bag    |      759253542 | janetjones@yahoo.com |
|                 2 | janet smith jones | female | 1980-06-23    | Melrose 123            | NULL           |              1 | jj@fstreet.com       |
|                 4 | Gloaria Williams  | female | 1984-02-14    | 2nd street 23          | NULL           |              1 | NULL                 |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
3 rows in set (0.001 sec)

MariaDB [demo]> SELECT COUNT(membership_number), gender FROM members GROUP BY gender HAVING COUNT(membership_number) > 2;  /*greater than using diffrent type query*/
+--------------------------+--------+
| COUNT(membership_number) | gender |
+--------------------------+--------+
|                        3 | female |
+--------------------------+--------+
1 row in set (0.002 sec)

MariaDB [demo]> SELECT COUNT(membership_number), gender FROM members GROUP BY gender HAVING COUNT(membership_number) <> 2;  /*not equal  using diffrent type query*/
+--------------------------+--------+
| COUNT(membership_number) | gender |
+--------------------------+--------+
|                        3 | female |
|                        1 | Male   |
+--------------------------+--------+
2 rows in set (0.001 sec)

MariaDB [demo]> SELECT COUNT(membership_number), gender FROM members GROUP BY gender HAVING COUNT(membership_number) < 4;  /*less than using diffrent type query*/
+--------------------------+--------+
| COUNT(membership_number) | gender |
+--------------------------+--------+
|                        3 | female |
|                        1 | Male   |
+--------------------------+--------+
2 rows in set (0.001 sec)

MariaDB [demo]> SELECT COUNT(membership_number), gender FROM members HAVING gender="female";  /*equal  using diffrent type query*/
+--------------------------+--------+
| COUNT(membership_number) | gender |
+--------------------------+--------+
|                        4 | female |
+--------------------------+--------+
1 row in set (0.001 sec)

MariaDB [demo]>

/*5.Create insert query in Member Table... */

MariaDB [demo]> INSERT INTO `members` (`membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email`) VALUES
    -> (1, 'janes_notes', 'female', '1980-07-21', 'First Street Plot No 4', 'Private Bag', 759253542, 'janetjones@yahoo.com'),
    -> (2, 'janet smith jones', 'female', '1980-06-23', 'Melrose 123', 'NULL', 1, 'jj@fstreet.com'),
    -> (3, 'Robert Fill', 'Male', '1980-07-12', '3rd street 34', 'NULL', 12345, 'rm@street.com'),
    -> (4, 'Gloaria Williams', 'female', '1984-02-14', '2nd street 23', 'NULL', 1, 'NULL');
Query OK, 4 rows affected (0.006 sec)
Records: 4  Duplicates: 0  Warnings: 0

MariaDB [demo]> INSERT INTO `movies` (`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES
    -> (1, 'Pirates of the Carib', 'Rob Marshell', 2011, '1'),
    -> (2, 'Forgetting Sarah Mar', 'Nocholas  stoller', 2008, '2'),
    -> (3, 'X-MEN', 'NULL', 2008, 'NULL'),
    -> (4, 'CODE-NAME-BLACK', 'Edgar Jimz', 2010, 'NULL'),
    -> (5, 'DADDAY\'S LITTLE GIRL', 'NULL', 2007, '8'),
    -> (6, 'Angels and Demons', 'NULL', 2007, '6'),
    -> (7, 'Divinci Code', 'NULL', 2007, '6'),
    -> (9, 'Honey Monners', 'John Schultz', 2005, '8'),
    -> (16, '67%Guilty', 'NULL', 2012, 'NULL');
Query OK, 9 rows affected (0.006 sec)
Records: 9  Duplicates: 0  Warnings: 0

/*6.Get record form members table using select query*/
MariaDB [demo]> SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM `members`;
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
| membership_number | full_names        | gender | date_of_birth | physical_address       | postal_address | contact_number | email                |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
|                 1 | janes_notes       | female | 1980-07-21    | First Street Plot No 4 | Private Bag    |      759253542 | janetjones@yahoo.com |
|                 2 | janet smith jones | female | 1980-06-23    | Melrose 123            | NULL           |              1 | jj@fstreet.com       |
|                 3 | Robert Fill       | Male   | 1980-07-12    | 3rd street 34          | NULL           |          12345 | rm@street.com        |
|                 4 | Gloaria Williams  | female | 1984-02-14    | 2nd street 23          | NULL           |              1 | NULL                 |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
4 rows in set (0.001 sec)

/*7.Getting only the full_names, gender, physical_address and email fields only from memeber table*/
MariaDB [demo]> SELECT  `full_names`, `gender`, `physical_address`,`email` FROM `members`;
+-------------------+--------+------------------------+----------------------+
| full_names        | gender | physical_address       | email                |
+-------------------+--------+------------------------+----------------------+
| janes_notes       | female | First Street Plot No 4 | janetjones@yahoo.com |
| janet smith jones | female | Melrose 123            | jj@fstreet.com       |
| Robert Fill       | Male   | 3rd street 34          | rm@street.com        |
| Gloaria Williams  | female | 2nd street 23          | NULL                 |
+-------------------+--------+------------------------+----------------------+
4 rows in set (0.000 sec)

/*8.Get a member's personal details from members table given the membership number 1*/
MariaDB [demo]> SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM `members` WHERE membership_number='1';
+-------------------+-------------+--------+---------------+------------------------+----------------+----------------+----------------------+
| membership_number | full_names  | gender | date_of_birth | physical_address       | postal_address | contact_number | email                |
+-------------------+-------------+--------+---------------+------------------------+----------------+----------------+----------------------+
|                 1 | janes_notes | female | 1980-07-21    | First Street Plot No 4 | Private Bag    |      759253542 | janetjones@yahoo.com |
+-------------------+-------------+--------+---------------+------------------------+----------------+----------------+----------------------+
1 row in set (0.001 sec)

/*9.Get a list of all the movies in category 2 that were released in 2008*/
MariaDB [demo]> SELECT `movie_id`,`title`,`year_released`FROM movies WHERE year_released='2008';
+----------+----------------------+---------------+
| movie_id | title                | year_released |
+----------+----------------------+---------------+
|        2 | Forgetting Sarah Mar |          2008 |
|        3 | X-MEN                |          2008 |
+----------+----------------------+---------------+
2 rows in set (0.001 sec)

MariaDB [demo]> SELECT `movie_id`,`title`,`year_released` FROM movies WHERE year_released LIKE '%8'; /*use other query*/
+----------+----------------------+---------------+
| movie_id | title                | year_released |
+----------+----------------------+---------------+
|        2 | Forgetting Sarah Mar |          2008 |
|        3 | X-MEN                |          2008 |
+----------+----------------------+---------------+
2 rows in set (0.001 sec)

/*10.Gets all the movies in either category 1 or category 2*/
MariaDB [demo]> SELECT `movie_id`,`title`,`year_released`,`director`,`category_id` FROM movies WHERE category_id='1' OR category_id='2';
+----------+----------------------+---------------+-------------------+-------------+
| movie_id | title                | year_released | director          | category_id |
+----------+----------------------+---------------+-------------------+-------------+
|        1 | Pirates of the Carib |          2011 | Rob Marshell      | 1           |
|        2 | Forgetting Sarah Mar |          2008 | Nocholas  stoller | 2           |
+----------+----------------------+---------------+-------------------+-------------+
2 rows in set (0.001 sec)

/*11.Gets rows where membership_number is either 1 , 2 or 3*/

MariaDB [demo]> SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM members WHERE  membership_number < 4 ;
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
| membership_number | full_names        | gender | date_of_birth | physical_address       | postal_address | contact_number | email                |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
|                 1 | janes_notes       | female | 1980-07-21    | First Street Plot No 4 | Private Bag    |      759253542 | janetjones@yahoo.com |
|                 2 | janet smith jones | female | 1980-06-23    | Melrose 123            | NULL           |              1 | jj@fstreet.com       |
|                 3 | Robert Fill       | Male   | 1980-07-12    | 3rd street 34          | NULL           |          12345 | rm@street.com        |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
3 rows in set (0.001 sec)

/*12.Gets all the female members from the members table using the equal to comparison operator.*/

MariaDB [demo]> SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM `members` WHERE `gender` = 'female';
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
| membership_number | full_names        | gender | date_of_birth | physical_address       | postal_address | contact_number | email                |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
|                 1 | janes_notes       | female | 1980-07-21    | First Street Plot No 4 | Private Bag    |      759253542 | janetjones@yahoo.com |
|                 2 | janet smith jones | female | 1980-06-23    | Melrose 123            | NULL           |              1 | jj@fstreet.com       |
|                 4 | Gloaria Williams  | female | 1984-02-14    | 2nd street 23          | NULL           |              1 | NULL                 |
+-------------------+-------------------+--------+---------------+------------------------+----------------+----------------+----------------------+
3 rows in set (0.000 sec)


/*13.Gets all the payments that are greater than 2,000 from the payments table*/
MariaDB [demo]> INSERT INTO `payment` (`payment_id`, `fname`, `lname`, `amount`, `payment_date`, `received_payment`) VALUES
    -> (1, 'raj', 'joshi', 100, '2020-01-01', '2020-01-04'),
    -> (2, 'bony', 'patel', 1000, '2020-01-02', '2020-01-07'),
    -> (3, 'kiran', 'patel', 200, '2020-01-02', '2020-01-07'),
    -> (4, 'sivani', 'patel', 3500, '2020-01-02', '2020-01-07'),
    -> (5, 'kishan', 'patel', 4000, '2020-01-02', '2020-01-07'),
    -> (6, 'dhara', 'patel', 2500, '2020-01-02', '2020-01-07');
Query OK, 6 rows affected (0.003 sec)
Records: 6  Duplicates: 0  Warnings: 0

MariaDB [demo]> SELECT `payment_id`,`fname`,`lname`,`amount`,`payment_date`,`received_payment`FROM `payment` WHERE amount>2000;
+------------+--------+-------+--------+--------------+------------------+
| payment_id | fname  | lname | amount | payment_date | received_payment |
+------------+--------+-------+--------+--------------+------------------+
|          4 | sivani | patel |   3500 | 2020-01-02   | 2020-01-07       |
|          5 | kishan | patel |   4000 | 2020-01-02   | 2020-01-07       |
|          6 | dhara  | patel |   2500 | 2020-01-02   | 2020-01-07       |
+------------+--------+-------+--------+--------------+------------------+
3 rows in set (0.000 sec)